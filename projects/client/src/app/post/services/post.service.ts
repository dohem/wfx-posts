import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { Post, PostCreateOrUpdate } from '../models/post.model';
import { PostRemoteService } from './post-remote.service';
import { PostMockService } from './post-mock.service';

@Injectable()
export class PostService {

  private useMockService = false;  // Set true for development
  private service: PostRemoteService | PostMockService;

  constructor(
    private readonly remoteService: PostRemoteService,
    private readonly mockService: PostMockService,
  ) {
    this.service = (this.useMockService) ? this.mockService : this.remoteService;
  }

  public getPosts(): Observable<Post[]> {
    return this.service.getPosts();
  }

  public getPost(id: number): Observable<Post> {
    return this.service.getPost(id);
  }

  public createPost(data: PostCreateOrUpdate): Observable<Post> {
    return this.service.postPost(data);
  }

  public updatePost(id: number, data: PostCreateOrUpdate): Observable<Post> {
    return this.service.putPost(id, data);
  }

  public deletePost(id: number): Observable<Post> {
    return this.service.deletePost(id);
  }
}
