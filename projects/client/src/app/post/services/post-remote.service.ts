import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Post, PostCreateOrUpdate } from '../models/post.model';


/*
 * Improvements:
 * - move httpOptions and errorHandler to a BaseService to extend here
*/
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class PostRemoteService {

  private url = 'https://wf-challenge-ldeltwzgmn.herokuapp.com/api/v1/posts';

  constructor(
    private http: HttpClient,
  ) { }

  public getPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.url)
      .pipe(
        catchError(this.errorHandler<Post[]>([]))
      );
  }

  public getPost(id: number): Observable<Post> {
    return this.http.get<Post>(`${this.url}/${id}`)
      .pipe(
        catchError(this.errorHandler<Post>())
      );
  }

  public postPost(post: PostCreateOrUpdate): Observable<Post> {
    return this.http.post<Post>(this.url, post, httpOptions)
      .pipe(
        catchError(this.errorHandler<Post>())
      );
  }

  public putPost(id: number, post: PostCreateOrUpdate): Observable<any> {
    return this.http.put(`${this.url}/${id}`, post, httpOptions)
      .pipe(
        catchError(this.errorHandler<Post>())
      );
  }

  public deletePost(id: number): Observable<Post> {
    return this.http.delete<Post>(`${this.url}/${id}`, httpOptions)
      .pipe(
        catchError(this.errorHandler<Post>())
      );
  }

  private errorHandler<T>(result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
}
