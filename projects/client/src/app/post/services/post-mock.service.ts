import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { Post, PostCreateOrUpdate } from '../models/post.model';
import { realPostMocks } from '../mocks/posts.mock';


@Injectable()
export class PostMockService {
  constructor() { }

  public getPosts(): Observable<Post[]> {
    console.log('mocked getPosts');
    return of(realPostMocks).pipe(delay(800));
  }

  public getPost(id: number): Observable<Post> {
    console.log('mocked getPost', id);
    return of(realPostMocks.find((mock) => mock.id === id)).pipe(delay(300));
  }

  public postPost(post: PostCreateOrUpdate): Observable<Post> {
    console.log('mocked createPost', post);
    return of(realPostMocks.find((mock) => mock.id === 1)).pipe(delay(300));
  }

  public putPost(id: number, post: PostCreateOrUpdate): Observable<any> {
    console.log('mocked updatePost', id, post);
    return of(realPostMocks.find((mock) => mock.id === (id % 3) + 1)).pipe(delay(300));
  }

  public deletePost(id: number): Observable<Post> {
    console.log('mocked deletePost', id);
    return of(null).pipe(delay(300));
  }
}
