import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Post, PostCreateOrUpdate } from '../../models/post.model';
import { PostService } from '../../services/post.service';

@Component({
  selector: 'post-edit',
  templateUrl: './post-edit.component.html',
  styleUrls: ['./post-edit.component.css']
})
export class PostEditComponent implements OnInit {

  private mode: 'add' | 'edit';
  public title: string;
  public post: Post;
  public form: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private readonly postService: PostService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    const postId = +this.route.snapshot.paramMap.get('id');
    this.setMode(postId);
    this.buildForm();
    this.loadData(postId);
  }

  private setMode(id: number) {
    this.mode = (!!id) ? 'edit' : 'add';
  }

  private loadData(id: number) {
    if (this.mode === 'edit') {
      this.postService.getPost(id)
        .subscribe((post) => {
          this.updateState(post);
        });
    } else {
      this.setTitle();
    }
  }

  private updateState(post: Post) {
    this.post = post;
    this.form.patchValue({
      ...this.post
    });
    this.setTitle(post);
  }

  private buildForm() {
    this.form = this.fb.group({
      title: ['', Validators.required],
      content: ['', Validators.required],
      lat: [''],
      long: [''],
      image_url: [''],
    });
  }

  private setTitle(post?: Post) {
    this.title = (!!post) ? `Edit post: ${ post.title }` : 'Add a new post';
  }

  public onSave() {
    const data: PostCreateOrUpdate = this.form.getRawValue();

    if (this.mode === 'edit') {
      this.postService.updatePost(this.post.id, data)
        .subscribe((post) => this.onSaved(post));
    } else {
      this.postService.createPost(data)
        .subscribe((post) => this.onSaved(post));
    }
  }

  private onSaved(post: Post) {
    this.updateState(post);
    this.router.navigate([`/posts`]);
  }

}
