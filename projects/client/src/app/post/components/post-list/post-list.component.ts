import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { PostService } from '../../services/post.service';
import { Post } from '../../models/post.model';
import { ConfirmDialogComponent } from '../../../shared/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {

  public posts: Post[];
  public columns: string[] = ['id', 'title', 'created_at', 'updated_at', 'actions'];

  constructor(
    private readonly postService: PostService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.loadData();
  }

  private loadData() {
    this.postService.getPosts()
      .subscribe((posts) => this.posts = posts);
  }

  public onDeletePost(id: number) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (!!result) {
        this.postService.deletePost(id)
          .subscribe((posts) => this.loadData());
      }
    });
  }

}
