import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Post } from '../../models/post.model';
import { PostService } from '../../services/post.service';

@Component({
  selector: 'post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {

  public post: Post;

  constructor(
    private route: ActivatedRoute,
    private readonly postService: PostService,
  ) { }

  ngOnInit() {
    this.loadData();
  }

  private loadData() {
    const postId = +this.route.snapshot.paramMap.get('id');
    this.postService.getPost(postId)
      .subscribe((post) => this.post = post);
  }

}
