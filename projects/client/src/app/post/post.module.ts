import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { PostListComponent } from './components/post-list/post-list.component';
import { PostDetailsComponent } from './components/post-details/post-details.component';
import { PostService } from './services/post.service';
import { SharedModule } from '../shared/shared.module';
import { PostRemoteService } from './services/post-remote.service';
import { PostMockService } from './services/post-mock.service';
import { PostEditComponent } from './components/post-edit/post-edit.component';

const components = [
  PostListComponent,
  PostDetailsComponent,
  PostEditComponent
];

@NgModule({
  declarations: components,
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    ReactiveFormsModule
  ],
  exports: components,
  providers: [
    PostService,
    PostRemoteService,
    PostMockService
  ]
})
export class PostModule { }
