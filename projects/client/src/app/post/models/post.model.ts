
export interface PostCreateOrUpdate {
  title: string;
  content: string;
  lat?: string;
  long?: string;
  image_url?: string;
}

export interface Post extends PostCreateOrUpdate {
  id: number;
  created_at: string;
  updated_at: string;
}
