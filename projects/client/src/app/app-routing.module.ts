import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PostDetailsComponent } from './post/components/post-details/post-details.component';
import { PostListComponent } from './post/components/post-list/post-list.component';
import { PostEditComponent } from './post/components/post-edit/post-edit.component';

const routes: Routes = [
  {
    path: '', redirectTo: '/posts', pathMatch: 'full'
  },
  {
    path: 'posts',
    children: [
      {
        path: '',
        component: PostListComponent
      },
      {
        path: 'new',
        component: PostEditComponent
      },
      {
        path: ':id',
        children: [
          {
            path: '', redirectTo: 'view', pathMatch: 'full'
          },
          {
            path: 'view',
            component: PostDetailsComponent
          },
          {
            path: 'edit',
            component: PostEditComponent
          }
        ]
      }
    ]
  }
  ,
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
