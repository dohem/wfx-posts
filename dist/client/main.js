(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "../../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _post_components_post_details_post_details_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./post/components/post-details/post-details.component */ "./src/app/post/components/post-details/post-details.component.ts");
/* harmony import */ var _post_components_post_list_post_list_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./post/components/post-list/post-list.component */ "./src/app/post/components/post-list/post-list.component.ts");
/* harmony import */ var _post_components_post_edit_post_edit_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./post/components/post-edit/post-edit.component */ "./src/app/post/components/post-edit/post-edit.component.ts");






var routes = [
    {
        path: '', redirectTo: '/posts', pathMatch: 'full'
    },
    {
        path: 'posts',
        children: [
            {
                path: '',
                component: _post_components_post_list_post_list_component__WEBPACK_IMPORTED_MODULE_4__["PostListComponent"]
            },
            {
                path: 'new',
                component: _post_components_post_edit_post_edit_component__WEBPACK_IMPORTED_MODULE_5__["PostEditComponent"]
            },
            {
                path: ':id',
                children: [
                    {
                        path: '', redirectTo: 'view', pathMatch: 'full'
                    },
                    {
                        path: 'view',
                        component: _post_components_post_details_post_details_component__WEBPACK_IMPORTED_MODULE_3__["PostDetailsComponent"]
                    },
                    {
                        path: 'edit',
                        component: _post_components_post_edit_post_edit_component__WEBPACK_IMPORTED_MODULE_5__["PostEditComponent"]
                    }
                ]
            }
        ]
    },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9qZWN0cy9jbGllbnQvc3JjL2FwcC9hcHAuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "../../node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _post_post_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./post/post.module */ "./src/app/post/post.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");







var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
                _post_post_module__WEBPACK_IMPORTED_MODULE_4__["PostModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/post/components/post-details/post-details.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/post/components/post-details/post-details.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".back-button {\r\n  margin-top: 10px;\r\n  margin-bottom: 20px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2plY3RzL2NsaWVudC9zcmMvYXBwL3Bvc3QvY29tcG9uZW50cy9wb3N0LWRldGFpbHMvcG9zdC1kZXRhaWxzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQkFBZ0I7RUFDaEIsbUJBQW1CO0FBQ3JCIiwiZmlsZSI6InByb2plY3RzL2NsaWVudC9zcmMvYXBwL3Bvc3QvY29tcG9uZW50cy9wb3N0LWRldGFpbHMvcG9zdC1kZXRhaWxzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmFjay1idXR0b24ge1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/post/components/post-details/post-details.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/post/components/post-details/post-details.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<button class=\"back-button\" mat-button mat-raised-button routerLink=\"/posts\">\n  <mat-icon>keyboard_backspace</mat-icon> Back to list\n</button>\n\n<mat-card *ngIf=\"post; else loader\">\n  <mat-card-header>\n    <mat-card-title>\n      {{ post.title }}\n    </mat-card-title>\n  </mat-card-header>\n  <img *ngIf=\"post.image_url\" mat-card-image [src]=\"post.image_url\">\n  <mat-card-content>\n    {{ post.content }}\n  </mat-card-content>\n</mat-card>\n\n<ng-template #loader>\n  <mat-spinner></mat-spinner>\n</ng-template>\n"

/***/ }),

/***/ "./src/app/post/components/post-details/post-details.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/post/components/post-details/post-details.component.ts ***!
  \************************************************************************/
/*! exports provided: PostDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostDetailsComponent", function() { return PostDetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "../../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_post_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/post.service */ "./src/app/post/services/post.service.ts");




var PostDetailsComponent = /** @class */ (function () {
    function PostDetailsComponent(route, postService) {
        this.route = route;
        this.postService = postService;
    }
    PostDetailsComponent.prototype.ngOnInit = function () {
        this.loadData();
    };
    PostDetailsComponent.prototype.loadData = function () {
        var _this = this;
        var postId = +this.route.snapshot.paramMap.get('id');
        this.postService.getPost(postId)
            .subscribe(function (post) { return _this.post = post; });
    };
    PostDetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'post-details',
            template: __webpack_require__(/*! ./post-details.component.html */ "./src/app/post/components/post-details/post-details.component.html"),
            styles: [__webpack_require__(/*! ./post-details.component.css */ "./src/app/post/components/post-details/post-details.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_post_service__WEBPACK_IMPORTED_MODULE_3__["PostService"]])
    ], PostDetailsComponent);
    return PostDetailsComponent;
}());



/***/ }),

/***/ "./src/app/post/components/post-edit/post-edit.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/post/components/post-edit/post-edit.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".back-button {\r\n  margin-top: 10px;\r\n  margin-bottom: 20px;\r\n}\r\n\r\n.full-width {\r\n  width: 100%;\r\n}\r\n\r\n.half-width {\r\n  width: 50%;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2plY3RzL2NsaWVudC9zcmMvYXBwL3Bvc3QvY29tcG9uZW50cy9wb3N0LWVkaXQvcG9zdC1lZGl0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQkFBZ0I7RUFDaEIsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0UsV0FBVztBQUNiOztBQUVBO0VBQ0UsVUFBVTtBQUNaIiwiZmlsZSI6InByb2plY3RzL2NsaWVudC9zcmMvYXBwL3Bvc3QvY29tcG9uZW50cy9wb3N0LWVkaXQvcG9zdC1lZGl0LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmFjay1idXR0b24ge1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuLmZ1bGwtd2lkdGgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uaGFsZi13aWR0aCB7XHJcbiAgd2lkdGg6IDUwJTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/post/components/post-edit/post-edit.component.html":
/*!********************************************************************!*\
  !*** ./src/app/post/components/post-edit/post-edit.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<button class=\"back-button\" mat-button mat-raised-button routerLink=\"/posts\">\n  <mat-icon>keyboard_backspace</mat-icon> Back to list\n</button>\n\n<mat-card *ngIf=\"!!mode; else loader\">\n  <mat-card-header>\n    <mat-card-title>\n      {{ title }}\n    </mat-card-title>\n  </mat-card-header>\n  <mat-card-content>\n    <form [formGroup]=\"form\">\n        <mat-form-field class=\"full-width\">\n          <input matInput placeholder=\"Title*\" formControlName=\"title\">\n        </mat-form-field>\n\n        <mat-form-field class=\"full-width\">\n          <textarea matInput placeholder=\"Content*\" formControlName=\"content\"></textarea>\n        </mat-form-field>\n\n        <mat-form-field class=\"half-width\">\n          <input matInput placeholder=\"Latitude\" formControlName=\"lat\">\n        </mat-form-field>\n\n        <mat-form-field class=\"half-width\">\n          <input matInput placeholder=\"Longitude\" formControlName=\"long\">\n        </mat-form-field>\n\n        <mat-form-field class=\"full-width\">\n          <input matInput placeholder=\"Image url\" formControlName=\"image_url\">\n        </mat-form-field>\n      </form>\n  </mat-card-content>\n\n  <mat-card-actions>\n    <button mat-button routerLink=\"/posts\">Cancel</button>\n    <button mat-button mat-raised-button color=\"primary\"\n      (click)=\"onSave()\" [disabled]=\"form.invalid\">\n      Save\n    </button>\n  </mat-card-actions>\n</mat-card>\n\n<ng-template #loader>\n  <mat-spinner></mat-spinner>\n</ng-template>\n"

/***/ }),

/***/ "./src/app/post/components/post-edit/post-edit.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/post/components/post-edit/post-edit.component.ts ***!
  \******************************************************************/
/*! exports provided: PostEditComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostEditComponent", function() { return PostEditComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "../../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "../../node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_post_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/post.service */ "./src/app/post/services/post.service.ts");





var PostEditComponent = /** @class */ (function () {
    function PostEditComponent(route, router, postService, fb) {
        this.route = route;
        this.router = router;
        this.postService = postService;
        this.fb = fb;
    }
    PostEditComponent.prototype.ngOnInit = function () {
        var postId = +this.route.snapshot.paramMap.get('id');
        this.setMode(postId);
        this.buildForm();
        this.loadData(postId);
    };
    PostEditComponent.prototype.setMode = function (id) {
        this.mode = (!!id) ? 'edit' : 'add';
    };
    PostEditComponent.prototype.loadData = function (id) {
        var _this = this;
        if (this.mode === 'edit') {
            this.postService.getPost(id)
                .subscribe(function (post) {
                _this.updateState(post);
            });
        }
        else {
            this.setTitle();
        }
    };
    PostEditComponent.prototype.updateState = function (post) {
        this.post = post;
        this.form.patchValue(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, this.post));
        this.setTitle(post);
    };
    PostEditComponent.prototype.buildForm = function () {
        this.form = this.fb.group({
            title: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            content: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            lat: [''],
            long: [''],
            image_url: [''],
        });
    };
    PostEditComponent.prototype.setTitle = function (post) {
        this.title = (!!post) ? "Edit post: " + post.title : 'Add a new post';
    };
    PostEditComponent.prototype.onSave = function () {
        var _this = this;
        var data = this.form.getRawValue();
        if (this.mode === 'edit') {
            this.postService.updatePost(this.post.id, data)
                .subscribe(function (post) { return _this.onSaved(post); });
        }
        else {
            this.postService.createPost(data)
                .subscribe(function (post) { return _this.onSaved(post); });
        }
    };
    PostEditComponent.prototype.onSaved = function (post) {
        this.updateState(post);
        this.router.navigate(["/posts"]);
    };
    PostEditComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'post-edit',
            template: __webpack_require__(/*! ./post-edit.component.html */ "./src/app/post/components/post-edit/post-edit.component.html"),
            styles: [__webpack_require__(/*! ./post-edit.component.css */ "./src/app/post/components/post-edit/post-edit.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _services_post_service__WEBPACK_IMPORTED_MODULE_4__["PostService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
    ], PostEditComponent);
    return PostEditComponent;
}());



/***/ }),

/***/ "./src/app/post/components/post-list/post-list.component.css":
/*!*******************************************************************!*\
  !*** ./src/app/post/components/post-list/post-list.component.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.add-button {\r\n  margin-top: 10px;\r\n  margin-bottom: 20px;\r\n}\r\n\r\n.post-table {\r\n  width: 100%;\r\n}\r\n\r\n.post-table th:first-child {\r\n  width: 40px;\r\n}\r\n\r\n.post-table th:last-child {\r\n  width: 200px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2plY3RzL2NsaWVudC9zcmMvYXBwL3Bvc3QvY29tcG9uZW50cy9wb3N0LWxpc3QvcG9zdC1saXN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBO0VBQ0UsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLFdBQVc7QUFDYjs7QUFFQTtFQUNFLFdBQVc7QUFDYjs7QUFFQTtFQUNFLFlBQVk7QUFDZCIsImZpbGUiOiJwcm9qZWN0cy9jbGllbnQvc3JjL2FwcC9wb3N0L2NvbXBvbmVudHMvcG9zdC1saXN0L3Bvc3QtbGlzdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbi5hZGQtYnV0dG9uIHtcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbn1cclxuXHJcbi5wb3N0LXRhYmxlIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLnBvc3QtdGFibGUgdGg6Zmlyc3QtY2hpbGQge1xyXG4gIHdpZHRoOiA0MHB4O1xyXG59XHJcblxyXG4ucG9zdC10YWJsZSB0aDpsYXN0LWNoaWxkIHtcclxuICB3aWR0aDogMjAwcHg7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/post/components/post-list/post-list.component.html":
/*!********************************************************************!*\
  !*** ./src/app/post/components/post-list/post-list.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<button class=\"add-button\" mat-button mat-raised-button routerLink=\"/posts/new\">\n  <mat-icon>add</mat-icon> Add a post\n</button>\n\n<mat-card>\n  <mat-card-header>\n    <mat-card-title>List of posts</mat-card-title>\n  </mat-card-header>\n  <mat-card-content>\n    <table *ngIf=\"posts; else loader\" mat-table [dataSource]=\"posts\" class=\"post-table\">\n      <ng-container matColumnDef=\"id\">\n        <th mat-header-cell *matHeaderCellDef>Id</th>\n        <td mat-cell *matCellDef=\"let element\">{{element.id}}</td>\n      </ng-container>\n\n      <ng-container matColumnDef=\"title\">\n        <th mat-header-cell *matHeaderCellDef>Title</th>\n        <td mat-cell *matCellDef=\"let element\">{{element.title}}</td>\n      </ng-container>\n\n      <ng-container matColumnDef=\"created_at\">\n        <th mat-header-cell *matHeaderCellDef>Creation</th>\n        <td mat-cell *matCellDef=\"let element\">{{element.created_at | date}}</td>\n      </ng-container>\n\n      <ng-container matColumnDef=\"updated_at\">\n        <th mat-header-cell *matHeaderCellDef>Last edition</th>\n        <td mat-cell *matCellDef=\"let element\">{{element.updated_at | date}}</td>\n      </ng-container>\n\n      <ng-container matColumnDef=\"actions\">\n        <th mat-header-cell *matHeaderCellDef>Actions</th>\n        <td mat-cell *matCellDef=\"let element\">\n            <button mat-icon-button color=\"primary\" routerLink=\"./{{element.id}}/view\">\n              <mat-icon>visibility</mat-icon>\n            </button>\n            <button mat-icon-button color=\"primary\" routerLink=\"./{{element.id}}/edit\">\n              <mat-icon>create</mat-icon>\n            </button>\n            <button mat-icon-button color=\"warn\" (click)=\"onDeletePost(element.id)\">\n              <mat-icon>delete</mat-icon>\n            </button>\n        </td>\n      </ng-container>\n\n      <tr mat-header-row *matHeaderRowDef=\"columns\"></tr>\n      <tr mat-row *matRowDef=\"let row; columns: columns;\"></tr>\n    </table>\n  </mat-card-content>\n</mat-card>\n\n<router-outlet></router-outlet>\n\n<ng-template #loader>\n  <mat-spinner></mat-spinner>\n</ng-template>\n\n"

/***/ }),

/***/ "./src/app/post/components/post-list/post-list.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/post/components/post-list/post-list.component.ts ***!
  \******************************************************************/
/*! exports provided: PostListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostListComponent", function() { return PostListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "../../node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _services_post_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/post.service */ "./src/app/post/services/post.service.ts");
/* harmony import */ var _shared_components_confirm_dialog_confirm_dialog_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared/components/confirm-dialog/confirm-dialog.component */ "./src/app/shared/components/confirm-dialog/confirm-dialog.component.ts");





var PostListComponent = /** @class */ (function () {
    function PostListComponent(postService, dialog) {
        this.postService = postService;
        this.dialog = dialog;
        this.columns = ['id', 'title', 'created_at', 'updated_at', 'actions'];
    }
    PostListComponent.prototype.ngOnInit = function () {
        this.loadData();
    };
    PostListComponent.prototype.loadData = function () {
        var _this = this;
        this.postService.getPosts()
            .subscribe(function (posts) { return _this.posts = posts; });
    };
    PostListComponent.prototype.onDeletePost = function (id) {
        var _this = this;
        var dialogRef = this.dialog.open(_shared_components_confirm_dialog_confirm_dialog_component__WEBPACK_IMPORTED_MODULE_4__["ConfirmDialogComponent"], {
            width: '250px',
        });
        dialogRef.afterClosed().subscribe(function (result) {
            if (!!result) {
                _this.postService.deletePost(id)
                    .subscribe(function (posts) { return _this.loadData(); });
            }
        });
    };
    PostListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'post-list',
            template: __webpack_require__(/*! ./post-list.component.html */ "./src/app/post/components/post-list/post-list.component.html"),
            styles: [__webpack_require__(/*! ./post-list.component.css */ "./src/app/post/components/post-list/post-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_post_service__WEBPACK_IMPORTED_MODULE_3__["PostService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"]])
    ], PostListComponent);
    return PostListComponent;
}());



/***/ }),

/***/ "./src/app/post/mocks/posts.mock.ts":
/*!******************************************!*\
  !*** ./src/app/post/mocks/posts.mock.ts ***!
  \******************************************/
/*! exports provided: postMocks, realPostMocks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "postMocks", function() { return postMocks; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "realPostMocks", function() { return realPostMocks; });
// TODO: us for testing
var postMocks = [
    {
        id: 218,
        title: 'Madrid',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
        created_at: '2019-04-23T15:48:32.640Z',
        updated_at: '2019-04-23T15:48:32.640Z'
    },
    {
        id: 219,
        title: 'Barcelona',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
        lat: '40.41678',
        long: '-3.70379',
        created_at: '2019-04-23T15:48:32.640Z',
        updated_at: '2019-04-23T15:48:32.640Z'
    },
    {
        id: 220,
        title: 'Burgos',
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
        lat: '41.41678',
        long: '-4.70379',
        image_url: 'https://c2.staticflickr.com/2/1269/4670777817_d657cd9819_b.jpg',
        created_at: '2019-04-23T15:48:32.640Z',
        updated_at: '2019-04-23T15:48:32.640Z'
    },
];
var realPostMocks = [
    {
        id: 1,
        title: 'Madrid',
        content: 'Madrid is the capital of Spain and the largest municipality in both the Community of Madrid and Spain as a whole.',
        lat: '40.41678',
        long: '-3.70379',
        image_url: 'https://c2.staticflickr.com/2/1269/4670777817_d657cd9819_b.jpg',
        created_at: '2019-04-23T15:48:32.640Z',
        updated_at: '2019-04-23T15:48:32.640Z'
    },
    {
        id: 2,
        title: 'Barcelona',
        content: 'Barcelona is the capital and largest city of Catalonia with a population of 1.6 million within city limits.',
        lat: '41.3851',
        long: '2.1734',
        image_url: 'https://static.independent.co.uk/s3fs-public/styles/story_medium/public/thumbnails/image/2017/05/17/15/barcelona-skyline.jpg',
        created_at: '2019-04-23T15:48:32.647Z',
        updated_at: '2019-04-23T15:48:32.647Z'
    },
    {
        id: 3,
        title: 'Berlin',
        content: 'Berlin is the capital and the largest city of Germany as well as one of its 16 constituent states. With a population of approximately 3.7 million, Berlin is the second...',
        lat: '52.5065133',
        long: '13.1445548',
        image_url: 'https://lonelyplanetwp.imgix.net/2015/12/brandenburg-gate-berlin-GettyRF-1500-cs.jpg',
        created_at: '2019-04-23T15:48:32.653Z',
        updated_at: '2019-04-23T15:48:32.653Z'
    }
];


/***/ }),

/***/ "./src/app/post/post.module.ts":
/*!*************************************!*\
  !*** ./src/app/post/post.module.ts ***!
  \*************************************/
/*! exports provided: PostModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostModule", function() { return PostModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "../../node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "../../node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "../../node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _components_post_list_post_list_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/post-list/post-list.component */ "./src/app/post/components/post-list/post-list.component.ts");
/* harmony import */ var _components_post_details_post_details_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/post-details/post-details.component */ "./src/app/post/components/post-details/post-details.component.ts");
/* harmony import */ var _services_post_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./services/post.service */ "./src/app/post/services/post.service.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _services_post_remote_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./services/post-remote.service */ "./src/app/post/services/post-remote.service.ts");
/* harmony import */ var _services_post_mock_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/post-mock.service */ "./src/app/post/services/post-mock.service.ts");
/* harmony import */ var _components_post_edit_post_edit_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/post-edit/post-edit.component */ "./src/app/post/components/post-edit/post-edit.component.ts");












var components = [
    _components_post_list_post_list_component__WEBPACK_IMPORTED_MODULE_5__["PostListComponent"],
    _components_post_details_post_details_component__WEBPACK_IMPORTED_MODULE_6__["PostDetailsComponent"],
    _components_post_edit_post_edit_component__WEBPACK_IMPORTED_MODULE_11__["PostEditComponent"]
];
var PostModule = /** @class */ (function () {
    function PostModule() {
    }
    PostModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: components,
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_8__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]
            ],
            exports: components,
            providers: [
                _services_post_service__WEBPACK_IMPORTED_MODULE_7__["PostService"],
                _services_post_remote_service__WEBPACK_IMPORTED_MODULE_9__["PostRemoteService"],
                _services_post_mock_service__WEBPACK_IMPORTED_MODULE_10__["PostMockService"]
            ]
        })
    ], PostModule);
    return PostModule;
}());



/***/ }),

/***/ "./src/app/post/services/post-mock.service.ts":
/*!****************************************************!*\
  !*** ./src/app/post/services/post-mock.service.ts ***!
  \****************************************************/
/*! exports provided: PostMockService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostMockService", function() { return PostMockService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "../../node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "../../node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _mocks_posts_mock__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../mocks/posts.mock */ "./src/app/post/mocks/posts.mock.ts");





var PostMockService = /** @class */ (function () {
    function PostMockService() {
    }
    PostMockService.prototype.getPosts = function () {
        console.log('mocked getPosts');
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(_mocks_posts_mock__WEBPACK_IMPORTED_MODULE_4__["realPostMocks"]).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(800));
    };
    PostMockService.prototype.getPost = function (id) {
        console.log('mocked getPost', id);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(_mocks_posts_mock__WEBPACK_IMPORTED_MODULE_4__["realPostMocks"].find(function (mock) { return mock.id === id; })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(300));
    };
    PostMockService.prototype.postPost = function (post) {
        console.log('mocked createPost', post);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(_mocks_posts_mock__WEBPACK_IMPORTED_MODULE_4__["realPostMocks"].find(function (mock) { return mock.id === 1; })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(300));
    };
    PostMockService.prototype.putPost = function (id, post) {
        console.log('mocked updatePost', id, post);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(_mocks_posts_mock__WEBPACK_IMPORTED_MODULE_4__["realPostMocks"].find(function (mock) { return mock.id === (id % 3) + 1; })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(300));
    };
    PostMockService.prototype.deletePost = function (id) {
        console.log('mocked deletePost', id);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(null).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(300));
    };
    PostMockService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PostMockService);
    return PostMockService;
}());



/***/ }),

/***/ "./src/app/post/services/post-remote.service.ts":
/*!******************************************************!*\
  !*** ./src/app/post/services/post-remote.service.ts ***!
  \******************************************************/
/*! exports provided: PostRemoteService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostRemoteService", function() { return PostRemoteService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "../../node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "../../node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "../../node_modules/rxjs/_esm5/operators/index.js");





/*
 * Improvements:
 * - move httpOptions and errorHandler to a BaseService to extend here
*/
var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({ 'Content-Type': 'application/json' })
};
var PostRemoteService = /** @class */ (function () {
    function PostRemoteService(http) {
        this.http = http;
        this.url = 'https://wf-challenge-ldeltwzgmn.herokuapp.com/api/v1/posts';
    }
    PostRemoteService.prototype.getPosts = function () {
        return this.http.get(this.url)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorHandler([])));
    };
    PostRemoteService.prototype.getPost = function (id) {
        return this.http.get(this.url + "/" + id)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorHandler()));
    };
    PostRemoteService.prototype.postPost = function (post) {
        return this.http.post(this.url, post, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorHandler()));
    };
    PostRemoteService.prototype.putPost = function (id, post) {
        return this.http.put(this.url + "/" + id, post, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorHandler()));
    };
    PostRemoteService.prototype.deletePost = function (id) {
        return this.http.delete(this.url + "/" + id, httpOptions)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.errorHandler()));
    };
    PostRemoteService.prototype.errorHandler = function (result) {
        return function (error) {
            console.error(error);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["of"])(result);
        };
    };
    PostRemoteService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], PostRemoteService);
    return PostRemoteService;
}());



/***/ }),

/***/ "./src/app/post/services/post.service.ts":
/*!***********************************************!*\
  !*** ./src/app/post/services/post.service.ts ***!
  \***********************************************/
/*! exports provided: PostService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PostService", function() { return PostService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _post_remote_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./post-remote.service */ "./src/app/post/services/post-remote.service.ts");
/* harmony import */ var _post_mock_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./post-mock.service */ "./src/app/post/services/post-mock.service.ts");




var PostService = /** @class */ (function () {
    function PostService(remoteService, mockService) {
        this.remoteService = remoteService;
        this.mockService = mockService;
        this.useMockService = false; // Set true for development
        this.service = (this.useMockService) ? this.mockService : this.remoteService;
    }
    PostService.prototype.getPosts = function () {
        return this.service.getPosts();
    };
    PostService.prototype.getPost = function (id) {
        return this.service.getPost(id);
    };
    PostService.prototype.createPost = function (data) {
        return this.service.postPost(data);
    };
    PostService.prototype.updatePost = function (id, data) {
        return this.service.putPost(id, data);
    };
    PostService.prototype.deletePost = function (id) {
        return this.service.deletePost(id);
    };
    PostService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_post_remote_service__WEBPACK_IMPORTED_MODULE_2__["PostRemoteService"],
            _post_mock_service__WEBPACK_IMPORTED_MODULE_3__["PostMockService"]])
    ], PostService);
    return PostService;
}());



/***/ }),

/***/ "./src/app/shared/components/confirm-dialog/confirm-dialog.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/shared/components/confirm-dialog/confirm-dialog.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Are you sure?</h1>\r\n<div mat-dialog-actions>\r\n  <button mat-button (click)=\"onNoClick()\" cdkFocusInitial>Not really</button>\r\n  <button mat-button mat-raised-button color=\"warn\" [mat-dialog-close]=\"true\">Yes, sure</button>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/shared/components/confirm-dialog/confirm-dialog.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/shared/components/confirm-dialog/confirm-dialog.component.ts ***!
  \******************************************************************************/
/*! exports provided: ConfirmDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmDialogComponent", function() { return ConfirmDialogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "../../node_modules/@angular/material/esm5/material.es5.js");



var ConfirmDialogComponent = /** @class */ (function () {
    function ConfirmDialogComponent(dialogRef) {
        this.dialogRef = dialogRef;
    }
    ConfirmDialogComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    ConfirmDialogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'confirm-dialog.component',
            template: __webpack_require__(/*! ./confirm-dialog.component.html */ "./src/app/shared/components/confirm-dialog/confirm-dialog.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"]])
    ], ConfirmDialogComponent);
    return ConfirmDialogComponent;
}());



/***/ }),

/***/ "./src/app/shared/shared.module.ts":
/*!*****************************************!*\
  !*** ./src/app/shared/shared.module.ts ***!
  \*****************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "../../node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "../../node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "../../node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "../../node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _components_confirm_dialog_confirm_dialog_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/confirm-dialog/confirm-dialog.component */ "./src/app/shared/components/confirm-dialog/confirm-dialog.component.ts");






var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _components_confirm_dialog_confirm_dialog_component__WEBPACK_IMPORTED_MODULE_5__["ConfirmDialogComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatProgressSpinnerModule"]
            ],
            exports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatProgressSpinnerModule"]
            ],
            entryComponents: [_components_confirm_dialog_confirm_dialog_component__WEBPACK_IMPORTED_MODULE_5__["ConfirmDialogComponent"]]
        })
    ], SharedModule);
    return SharedModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "../../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "../../node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Adri\code\tests\wefox\code\wefox-frontend-challenge\projects\client\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map